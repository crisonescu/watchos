package com.cristianonescu.watchos;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * Created by Cristian on 25.02.2015.
 */
public class BTCommunicationLogic extends Service {


    private static final int REQUEST_ENABLE_BT = 1;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private OutputStream outStream = null;

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private String address = null;

    public static final long NOTIFY_INTERVAL = 1000; // 8 seconds

    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;

    private String txtView;
    private NotificationReceiver nReceiver;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startBTTransmission(intent);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        btAdapter = null;
        btSocket = null;
        outStream = null;


        address = null;

        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        }

        this.stopSelf();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
        //unregister notifications
        unregisterReceiver(nReceiver);
    }

    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;

    /**
     * interface for clients that bind
     */
    IBinder mBinder;

    /**
     * indicates whether onRebind should be used
     */
    boolean mAllowRebind;

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
        nReceiver = new NotificationReceiver();
    }


    /**
     * Called when all clients have unbound with unbindService()
     */
    @Override
    public boolean onUnbind(Intent intent) {

        Log.d("SEND BT SERIAL", "In onPause()");

        if (outStream != null) {
            try {
                outStream.flush();
            } catch (IOException e) {
                errorExit("Fatal Error", "In onPause() and failed to flush output stream: " + e.getMessage() + ". Unplug bluetooth module and then plug back.");
            }
        }

        try {
            btSocket.close();
        } catch (IOException e2) {
            errorExit("Fatal Error", "In onPause() and failed to close socket." + e2.getMessage() + ".");
        }

        return mAllowRebind;
    }

    /**
     * Called when a client is binding to the service with bindService()
     */
    @Override
    public void onRebind(Intent intent) {
        Log.d("SEND BT SERIAL", "In onResume - Attempting client connect");

        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        btAdapter.cancelDiscovery();

        Log.d("SEND BT SERIAL", "Connecting to Remote");
        try {
            btSocket.connect();
            Log.d("SEND BT SERIAL", "Connection established and data link opened...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        Log.d("SEND BT SERIAL", "Creating Socket");

        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
        }
    }


    private void checkBTState() {

        if (btAdapter == null) {
            errorExit("Fatal Error", "Bluetooth Not supported. Aborting.");
        } else {
            if (btAdapter.isEnabled()) {
                Log.d("SEND BT SERIAL", "Bluetooth is enabled");
            } else {
                Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
                //startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }
        }
    }

    private void errorExit(String title, String message) {
        Toast msg = Toast.makeText(getBaseContext(),
                title + " - " + message, Toast.LENGTH_LONG);
        msg.show();
        onDestroy();
    }

    private void startNotificationCatcher() {
        txtView = new String();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.cristianonescu.watchos.NOTIFICATION_LISTENER_EXAMPLE");
        registerReceiver(nReceiver, filter);

        Intent i = new Intent("com.cristianonescu.watchos.NOTIFICATION_LISTENER_SERVICE_EXAMPLE");
        i.putExtra("command", "list");
        sendBroadcast(i);
        //Toast.makeText(getApplicationContext(), i.toString(), Toast.LENGTH_SHORT).show();
    }

    private void clearNotificationCatcher() {
        Intent i = new Intent("com.kpbird.nlsexample.NOTIFICATION_LISTENER_SERVICE_EXAMPLE");
        i.putExtra("command", "clearall");
        sendBroadcast(i);
        txtView = new String();
    }

    private void startBTTransmission(Intent intent) {
        // Let it continue running until it is stopped.
        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();

        this.address = (String) intent.getExtras().get("pairedMac");


        btAdapter = BluetoothAdapter.getDefaultAdapter();
        checkBTState();

        ///
        Log.d("SEND BT SERIAL", "In onResume - Attempting client connect");

        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        btAdapter.cancelDiscovery();

        Log.d("SEND BT SERIAL", "Connecting to Remote");
        try {
            btSocket.connect();
            Log.d("SEND BT SERIAL", "Connection established and data link opened...");
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        Log.d("SEND BT SERIAL", "Creating Socket");

        try {
            outStream = btSocket.getOutputStream();
        } catch (IOException e) {
            errorExit("Fatal Error", "In onResume() and output stream creation failed:" + e.getMessage() + ".");
        }
///////USE THIS TIMER/////////////////
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
////////////////////OR THIS///////////
//        startNotificationCatcher();
//        sendData(new SimpleDateFormat("hh:mm:ss").format(new java.util.Date())); //<-send string
//        Toast.makeText(getApplicationContext(), txtView, Toast.LENGTH_SHORT).show();
        //clearNotificationCatcher();
////////////////////
    }

    private void sendData(String message) {
        byte[] msgBuffer = message.getBytes();

        Log.d("SEND BT SERIAL", "Sending data: " + message + "");

        try {
            outStream.write(msgBuffer);
            //Toast.makeText(getApplicationContext(), "DATA SHOULD BE SENT", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            String msg = "In onResume() and an exception occurred during write: " + e.getMessage();
            if (address.equals("00:00:00:00:00:00"))
                msg = msg + ".\n\nUpdate your server address.";
            msg = msg + ".\n\nCheck that the SPP UUID: " + MY_UUID.toString() + " exists on server.\n\n";

            errorExit("Fatal Error", msg);
        }
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    startNotificationCatcher();
                    sendData(new SimpleDateFormat("hh:mm:ss").format(new java.util.Date()) + "Timisoarafacebooknotif"); //<-send string
                    //clearNotificationCatcher();
                }

            });
        }

        @Override
        public boolean cancel() {
            clearNotificationCatcher();
            return super.cancel();
        }
    }

    class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String temp = intent.getStringExtra("notification_event") + "\n" + txtView;
                if(temp.contains("Notification List")
                        || temp.contains("============")
                        || temp.contains("android.systemui")
                        || temp.contains("cristianonescu.watchos")) {
                    //do nothing
                }else{
                    txtView = temp;
                }
                if(!txtView.isEmpty()) {
                    Toast.makeText(context, txtView, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
            }
        }
    }
}