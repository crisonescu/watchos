package com.cristianonescu.watchos;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Cristian on 11.09.2016.
 */
public class GeneralScreen extends Fragment {

    MainActivity context;

    public GeneralScreen(){

    }

    @SuppressLint("ValidFragment")
    public GeneralScreen(MainActivity mainActivity) {
        context = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.general_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toast.makeText(context, "GENERAL SCREEN", Toast.LENGTH_LONG).show();
    }
}
